/*
 * This file is part of the freeboot uefi bootloader.
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "uefi.h"
#include "ui.h"

extern struct efi_system_table* est;

void uputs(uint16_t *msg)
{
	est->con_out->output_string(est->con_out, msg);
}

void clear_screen(void)
{
	est->con_out->clear_screen();
}
