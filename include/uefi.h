#ifndef uefi_h
#define uefi_h

#include <stdint.h>

extern struct efi_system_table* est;

/* UEFI definitions, to avoid crappy hungarian notations */

struct efi_table_header {
	uint64_t signature;
	uint32_t revision;
	uint32_t headerSize;
	uint32_t crc32;
	uint32_t reserved;
};

struct efi_simple_text_output_protocol;

typedef uint64_t (*efi_text_string)(struct efi_simple_text_output_protocol* this, uint16_t* string);

struct efi_simple_text_output_protocol {
	uint64_t reset;
	efi_text_string output_string;
	uint64_t test_string;
	uint64_t query_mode;
	uint64_t set_mode;
	uint64_t set_attribute;
	uint64_t clear_screen;
	uint64_t set_cursor_position;
	uint64_t enable_cursor;
	uint64_t mode;
};

struct efi_system_table {
	struct efi_table_header hdr;
	int16_t* firmware_vendor;
	uint32_t firmware_revision;
	void* console_in_handle;
	uint64_t con_in;
	void* console_out_handle;
	struct efi_simple_text_output_protocol* con_out;
	void* standard_error_handle;
	uint64_t std_err;
	uint64_t runtime_services;
	uint64_t boot_services;
	uint64_t table_entries;
	uint64_t config_table;
};

#endif /* uefi_h */
