#!/bin/sh

PATH_OVMF=/usr/share/ovmf

qemu-system-x86_64 -cpu qemu64 \
    -bios ${PATH_OVMF}/OVMF.fd \
    -drive file=./uefi.img,if=ide -net none
