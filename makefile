# Note: please edit data.c as
# explained in https://wiki.osdev.org/UEFI_Bare_Bones

SRCDIR=src
OBJDIR=obj
BINDIR=.

BIN=bootx64.efi

SRCS:=$(wildcard $(SRCDIR)/*.c)
OBJS:=$(patsubst %.c,%.o,$(SRCS))
OBJS:=$(patsubst $(SRCDIR)%,$(OBJDIR)%,$(OBJS))

EFI_HDR=/usr/include/efi

CFLAGS=-target x86_64-unknown-windows \
	-ffreestanding \
	-fshort-wchar \
	-mno-red-zone \
	-Iinclude \
	-I$(EFI_HDR) -I$(EFI_HDR)/x86_64 -I$(EFI_HDR)/protocol

LDFLAGS=-target x86_64-unknown-windows \
	-nostdlib \
	-Wl,-entry:efi_main \
	-Wl,-subsystem:efi_application \
	-fuse-ld=lld-link

all: $(BINDIR)/$(BIN)

$(BINDIR)/$(BIN): $(OBJS) $(OBJDIR)/data.o
	clang $(LDFLAGS) $(OBJS) -o $(BINDIR)/$(BIN)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	clang $(CFLAGS) -c $< -o $@

$(OBJDIR)/data.o: /usr/lib/efi/data.c
	clang $(CFLAGS) -c $< -o $(OBJDIR)/data.o

clean:
	rm -rf $(BIN)
	rm -rf $(OBJDIR)/*

test:
	rm -rf uefi.img /tmp/part.img
	dd if=/dev/zero of=./uefi.img bs=512 count=93750
	parted uefi.img -s -a minimal mklabel gpt
	parted uefi.img -s -a minimal mkpart EFI FAT16 2048s 93716s
	parted uefi.img -s -a minimal toggle 1 boot
	dd if=/dev/zero of=/tmp/part.img bs=512 count=91669
	mformat -i /tmp/part.img -h 32 -t 32 -n 64 -c 1
	mcopy -i /tmp/part.img bootx64.efi ::
	dd if=/tmp/part.img of=uefi.img bs=512 count=91669 seek=2048 conv=notrunc

